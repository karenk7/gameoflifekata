package com.kkiener;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.kkiener.GameOfLife;

public class Main {
	
	public static void main(String[] args) throws FileNotFoundException {

		Scanner userInput = new Scanner(System.in);

		System.out.println("Welcome to Conway's Game of Life! \n " + "________________________________ \n \n"
				+ "You can customize how many rows and columns your game will have.\n");

		// User input to decide game rows and columns
		int rows = 0;
		int columns = 0;
		boolean validInput = false;
		do {
			try {
				System.out.println("How Many rows? \nPlease enter a number less than 40");
				rows = userInput.nextInt();
				userInput.nextLine();
				System.out.println("How Many columns? \nPlease enter a number less than 40  ");
				columns = userInput.nextInt();
				userInput.nextLine();
				if (rows < 40 && columns < 40) {
					validInput = true;
				}
			} catch (InputMismatchException e) {
				System.out.println("One of your numbers wasn't valid");
				userInput.nextLine(); // retrieving invalid character
			}

		} while (!validInput);

		GameOfLife playGame = new GameOfLife(rows, columns);

		// User input for updating the game
		validInput = false;
		String update = "y";
		do {
			while (update.equalsIgnoreCase("Y")) {

				System.out.println("Would you like to update the state of the game? Please enter Y or N \n");
				update = userInput.nextLine();
				if (update.equalsIgnoreCase("y")) {
					playGame.updateGameGridState();
				}
				if (update.equalsIgnoreCase("y") || update.equalsIgnoreCase("n")) {
					validInput = true;
				} else {
					System.out.println("Invalid input.");
					update = "y";
				}
			}
		} while (!validInput);

		System.out.println("Thanks for playing!");

	}
}