package com.kkiener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class GameOfLife {

	private int rows;
	private int columns;
	private String[][] gameGrid;
	private String liveCell = "O";
	private String deadCell = ".";
	private int updateCounter = 1;

	public GameOfLife(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		gameGrid = new String[rows][columns];
		populateInitialGridState();
	}

	public String[][] getGameGrid() {
		return gameGrid;
	}

	public int getGameRowCount() {
		return gameGrid.length;
	}

	public int getGameColumnCount() {
		return gameGrid[0].length;
	}

	public void populateInitialGridState() {
		Random randomGenerator = new Random();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				int randomInt = randomGenerator.nextInt();
				if (randomInt % 2 == 0) {
					gameGrid[i][j] = liveCell;
				} else {
					gameGrid[i][j] = deadCell;
				}
			}
		}
		System.out.println("Conway's Game of Life - Starting Grid State");
		System.out.println("===========================================");
		System.out.println();
		
		printGridToConsole();
		
		System.out.println();
		System.out.println("===========================================");
		System.out.println();
	}

	public void printGridToConsole() {
		for (int i = 0; i < getGameRowCount(); i++) {
			System.out.println(
					Arrays.deepToString(this.gameGrid[i]).replace('[', ' ').replace(']', ' ').replace(',', ' ').trim());
		}
		// learned about .deeptToString for 2D arrays
		// realized I needed to override toString to get rid of commas and add
		// tabs and found an example online
	}

	public List getNeighboringCellValues(int row, int col) {
		List<String> neighboringCells = new ArrayList<String>();

		// make sure i is in bounds
		for (int i = row - 1; i <= row + 1; i++) {
			if (i < 0 || i > this.getGameRowCount() - 1) {
				continue;
			}
			//make sure j is in bounds
			for (int j = col - 1; j <= col + 1; j++) {
				if (j < 0 || j > this.getGameColumnCount() - 1) {
					continue;
				}
				if (i == row && j == col) {
					continue;
				}
				neighboringCells.add(this.getGameGrid()[i][j]);
			}
		}
		return neighboringCells;
	}

	public int getNumberLiveNeighborCells(int row, int col) {
		
		int totalLiveCells = 0;

		for (int i = row - 1; i <= row + 1; i++) {
			if (i < 0 || i > this.getGameRowCount() - 1) {
				continue;
			}

			for (int j = col - 1; j <= col + 1; j++) {
				if (j < 0 || j > this.getGameColumnCount() - 1) {
					continue;
				}
				if (i == row && j == col) {
					continue;
				}
				String value = this.getGameGrid()[i][j];
				if (this.getGameGrid()[i][j] == "O") {
					totalLiveCells++;
				}
			}
		}
		return totalLiveCells;
	}

	public void updateGameGridState() {

		int startRow = 0;
		int startCol = 0;
		int maxRows = getGameRowCount();
		int maxCols = getGameColumnCount();
		int totalLiveCells = 0;

		for (int i = startRow; i < maxRows; i++) {
			for (int j = startCol; j < maxCols; j++) {
				totalLiveCells = getNumberLiveNeighborCells(i, j);
				if (this.getGameGrid()[i][j] == "O" && totalLiveCells < 2) {
					this.gameGrid[i][j] = ".";
				} else if (this.getGameGrid()[i][j] == "O" && totalLiveCells > 3) {
					this.gameGrid[i][j] = ".";
				} else if ((this.getGameGrid()[i][j] == "." && totalLiveCells == 3)) {
					this.gameGrid[i][j] = "O";
				}
			}
		}

		System.out.println("Conway's Game of Life - Updated Grid State - Update Round # " + updateCounter);
		System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		
		printGridToConsole();
		
		System.out.println();
		System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<");
		System.out.println();
		updateCounter++;
	}

}
