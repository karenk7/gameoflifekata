package com.kkiener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameOfLifeTest {
	
	private GameOfLife game;
	
	@Before
	public void setup() {
		game = new GameOfLife(6, 8);	
	}
	
	//Some of these tests helped me build the app, but as it evolved they became less relevant to the finished functionality.
	
	@Test
	public void test_game_grid_is_not_empty(){
				
		Assert.assertTrue(game.getGameGrid() != null);
	}
	
	@Test
	public void test_game_grid_has_6_rows() {
	
		Assert.assertEquals(6, game.getGameRowCount());
	}

	@Test
	public void test_game_grid_has_8_columns() {	
		
		Assert.assertEquals(8, game.getGameColumnCount());
	}
	
	@Test
	public void test_game_grid_contains_O_and_period(){
		
		game.populateInitialGridState();
		
		boolean containsO = false;
		boolean containsPeriod = false;
	
		for(int i = 0; i < game.getGameRowCount(); i++){
		        for(int j = 0; j < game.getGameColumnCount(); j++){
					if(game.getGameGrid()[i][j] == "O") {
						containsO = true;
					} else if (game.getGameGrid()[i][j] == ".") {
						containsPeriod = true;
		        }
		    }
		}
		
		Assert.assertTrue("expected to contain at least one O", containsO);
		Assert.assertTrue("expected to contain at least one period", containsPeriod);
	}
	
	@Test
	public void test_game_grid_does_not_contain_strings_other_than_O_or_period() {
		
		game.populateInitialGridState();
		
		boolean containsInvalidString = false;
		
		for(int i = 0; i < game.getGameRowCount(); i++){	
		        for(int j = 0; j < game.getGameColumnCount(); j++){
					if(game.getGameGrid()[i][j] != "O" && game.getGameGrid()[i][j] != ".") {
						containsInvalidString = true;
					}
		        }
		}	
		Assert.assertFalse("expected false", containsInvalidString);		
	}
	
	@Test 
	public void test_finding_first_live_cell() {
		game.populateInitialGridState();
		
		ArrayList<Integer> foundLiveCell = new ArrayList<Integer>();

		for(int i = 0; i < game.getGameRowCount(); i++){	
	        for(int j = 0; j < game.getGameColumnCount(); j++){
	        	if((game.getGameGrid()[i][j] == "O") && foundLiveCell.size() < 2){
						foundLiveCell.add(i);
						foundLiveCell.add(j);
						System.out.println("Live cell location" + foundLiveCell);
				}
	        }
		}
	
		Assert.assertEquals(2, foundLiveCell.size());	
	}
	
	@Test 
	public void test_finding_first_dead_cell() {
		game.populateInitialGridState();
		
		ArrayList<Integer> foundLiveCell = new ArrayList<Integer>();

		for(int i = 0; i < game.getGameRowCount(); i++){	
	        for(int j = 0; j < game.getGameColumnCount(); j++){
	        	if((game.getGameGrid()[i][j] == ".") && foundLiveCell.size() < 2){
						foundLiveCell.add(i);
						foundLiveCell.add(j);
						System.out.println("Dead cell location" + foundLiveCell);
				}
	        }
		}
	
		Assert.assertEquals(2, foundLiveCell.size());
	}
	
	@Test
	public void check_values_in_correct_number_of_neighboring_cells() {

		game.populateInitialGridState();

		Assert.assertEquals(3, game.getNeighboringCellValues(0, 0).size());
		Assert.assertEquals(5, game.getNeighboringCellValues(0, 4).size());
		Assert.assertEquals(8, game.getNeighboringCellValues(4, 4).size());	
	}
	
	@Test
	public void count_number_live_neighboring_cells() {

		game.populateInitialGridState();
		
		int results = game.getNumberLiveNeighborCells(1, 4);
		System.out.println("live cells = " + results);

		Assert.assertTrue(results > 0);			
	}
	
	@Test
	public void test_initializing_game_creates_array_with_6_rows_and_8_columns() {
		//new game initialized in @Before
	
		Assert.assertEquals("Expected 6", 6, game.getGameRowCount());
		Assert.assertEquals("Expected 8", 8, game.getGameColumnCount());
	}
	
	@Test
	public void test_initial_grid_matches_input_number_rows_and_columns() {
		GameOfLife gridTest = new GameOfLife(4, 6);
		
		Assert.assertEquals(4, gridTest.getGameRowCount());
		Assert.assertEquals(6, gridTest.getGameColumnCount());
	}		
	
	@Test
	public void test_updating_grid_state() {
		//this test needs work. Inconsistent results. It's not a definitive way to verify grid contents have changed.
		
		GameOfLife testGame = new GameOfLife(10, 10);
		
		int initialGameSample = testGame.getNumberLiveNeighborCells(0, 0);
		
		testGame.updateGameGridState();
		testGame.updateGameGridState();
		testGame.updateGameGridState();
		
		int updatedGameSample = testGame.getNumberLiveNeighborCells(0, 0);
		
		boolean gridsAreDifferent = initialGameSample == updatedGameSample;

		Assert.assertFalse(gridsAreDifferent);	
	}
	
}

